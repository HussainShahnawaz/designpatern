package com.ticketbooking;
import com.google.gson.JsonParseException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DashbBoar extends pageInit {

    public WebDriver driver;
    public DashbBoar(WebDriver driver) {
        super(driver);
        this.driver=driver;
    }

    @FindBy(id = "onetrust-accept-btn-handler")
    WebElement AcceptCookiee;

    @FindBy(id="from.search")
    WebElement Origin;

    @FindBy(id="to.search")
    WebElement Destination;

    @FindBy(xpath = "//*[@id=\"app\"]/div/div[1]/main/div[2]/div[4]/div/div/div[1]/section/form/div[3]/fieldset[1]/div[1]/button[2]")
    WebElement selectday;

//    @FindBy(id = "page.journeySearchForm.outbound.title")
//    WebElement time;
    @FindBy(className = "_nsi73u8")
    WebElement checkingprice;

    @FindBy(xpath = "//*[@data-test='cjs-price']")
    WebElement Totalprice;




    public void cookiee(){
        AcceptCookiee.click();
    }

    public void originfrom(){
        Origin.sendKeys("Gatwick Airport");
    }

    public void destinationto(){

        Destination.sendKeys("Leeds");
    }

    public void checkprice(){

        checkingprice.click();
    }

    public void dayselection(){
        selectday.click();
    }

    public void totalticketpric(){

        System.out.println(Totalprice.getText());
    }
}
