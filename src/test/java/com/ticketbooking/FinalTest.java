package com.ticketbooking;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class FinalTest {


    public static WebDriver driver;
    Properties prop;




        @BeforeMethod
        public void BrowserSetup() throws IOException {
        FileInputStream fileInput = new FileInputStream("/Users/shahnawaz/IcProjects/DesignPatternDemo/src/test/java/com/ticketbooking/data.Properties");
        prop = new Properties();
        prop.load(fileInput);

        if(prop.getProperty("browser").equals("chrome")){
            WebDriverManager.chromedriver().setup();
            driver = new ChromeDriver();


        }
        else {
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();


        }
    }



        @Test
        public void RunTest(){

        driver.get(prop.getProperty("url"));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        DashbBoar d = new DashbBoar(driver);
        d.cookiee();
        d.originfrom();
        d.destinationto();
        d.dayselection();
        d.checkprice();
        d.totalticketpric();



    }

        @AfterMethod
        public void FinalCall(){
            driver.quit();
        }
}
